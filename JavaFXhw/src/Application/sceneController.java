package Application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class sceneController implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;
    @FXML
    private Label newaccpwd_hint;
    @FXML
    private Label login_hint;
    @FXML
    private Label register_hint;
    @FXML
    private TextField register_acc;
    @FXML
    private PasswordField register_pwd;
    @FXML
    private TextField account;
    @FXML
    private PasswordField pwd;
    @FXML
    private TextField new_acc;
    @FXML
    private PasswordField new_pwd;
    @FXML
    private TextField new_net;
    @FXML
    private Label listview_result;
    @FXML
    private ListView<String> accpwd_listview;

    private static String file = "";
    private static String first_file = "";
    File login_file = new File("login_accpwd.txt");
    private static String accpwd[][] = new String[20][3];

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) { //初始化

        for (int x = 0; x < 20; x++) {
            if (x + 1 == 20) {
                continue;
            }
            if (accpwd[x + 1][2] == null && accpwd[x + 1][1] == null && accpwd[x + 1][0] == null) {
                accpwd[x + 1][0] = "";
                accpwd[x + 1][1] = "";
                accpwd[x + 1][2] = "";
            }
            accpwd_listview.getItems().addAll("網站：" + accpwd[x + 1][2] + "\n" + "帳號：" + accpwd[x + 1][0] + "\n" + "密碼：" + accpwd[x + 1][0] + "\n");
            accpwd_listview.refresh();
        }

        return;
    }

    @FXML
    public void switchToScene_init(ActionEvent event) throws IOException { //註冊 登出 >> 首頁 按鈕

        root = FXMLLoader.load(getClass().getResource("scene_init.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void switchToScene2(ActionEvent event) throws IOException { //首頁 >> 註冊 按鈕
        root = FXMLLoader.load(getClass().getResource("scene_2.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    public void switchToScene_login(ActionEvent event) throws IOException { //登入 >> 內頁 按鈕

        if (login_file.exists() && !login_file.isDirectory()) { //先判斷有沒有任何的註冊資料 沒有的話 後台也不會報錯
            FileReader fr = new FileReader("login_accpwd.txt");
            BufferedReader br = new BufferedReader(fr);
            String login[][] = new String[20][2];
            while (br.ready()) { //讀入登入用密碼檔案
                for (int i = 0; i < 20; i++) {
                    for (int j = 0; j < 2; j++) {
                        login[i][j] = br.readLine();
                    }
                }
            }
            fr.close();

            int i = 0;
            while (i <= 19) { //比對帳號密碼
                if (login[i][0] == null || account.getText().trim().isEmpty() || pwd.getText().trim().isEmpty()) {
                    login_hint.setText("帳號或密碼錯誤 請再試一次"); //遍歷後 無相同帳號
                    break;
                }
                if (login[i][0].equals(account.getText())) {
                    if (login[i][1].equals(pwd.getText())) {
                        file = login[i][0] + "_accpwd.txt"; //辨識帳號 取得對應帳號的密碼庫檔案
//                    System.out.print(file);

                        FileReader fr_login = new FileReader(file); //讀入密碼管理的資料
                        BufferedReader br_login = new BufferedReader(fr_login);
                        while (br_login.ready()) {
                            for (int x = 0; x < 20; x++) {
                                for (int y = 0; y < 3; y++) {
                                    if (x + 1 == 20) {
                                        continue;
                                    }
                                    if (accpwd[x][y] == null) {
                                        accpwd[x][y] = "";
                                    }
                                    accpwd[x][y] = br_login.readLine();
                                    accpwd_listview.getItems().addAll("網站：" + accpwd[x + 1][2] + "\n" + "帳號：" + accpwd[x + 1][0] + "\n" + "密碼：" + accpwd[x + 1][0] + "\n");
                                    accpwd_listview.refresh();
                                }
                            }
                        }
                        fr_login.close();

                        root = FXMLLoader.load(getClass().getResource("scene_login.fxml"));
                        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        scene = new Scene(root);
                        stage.setScene(scene);
                        stage.show();

                        break;

                    } else {
                        login_hint.setText("帳號或密碼錯誤 請再試一次"); //密碼錯誤
                        break;
                    }
                }
                i++;
            }
        } else {
            login_hint.setText("帳號或密碼錯誤 請再試一次");
        }

    }

    public static boolean validateLegalString(String content) { //檢查註冊帳號有無特殊字元
        String illegal = "`~!#%^&*= \\|{};:'\",<>/?○●★☆☉♀♂※¤╬の〆";
        for (int ii = 0; ii < content.length(); ii++) {
            for (int jj = 0; jj < illegal.length(); jj++) {
                if (content.charAt(ii) == illegal.charAt(jj)) {
                    return true;
                }
            }
        }
        return false;
    }

    @FXML
    private void register(ActionEvent event) throws IOException { //註冊 寫入帳號密碼
        if (register_acc.getText().trim().isEmpty() || register_pwd.getText().trim().isEmpty()) { //不可輸入空格
            register_hint.setText("請輸入帳號密碼");

        } else {

            if (validateLegalString(register_acc.getText())) {
                register_hint.setText("帳號不可使用特殊字元"); //不然檔案建不起來
            } else {
                FileWriter fw = new FileWriter("login_accpwd.txt", true);
                fw.append(register_acc.getText() + "\n");
                fw.append(register_pwd.getText() + "\n");
                fw.flush();
                fw.close();
                register_hint.setText("註冊成功 ! 返回首頁登入");

                first_file = register_acc.getText() + "_accpwd.txt"; //首次註冊建檔

                FileWriter fw_init = new FileWriter(first_file, true);

                fw_init.write("init\n");
                fw_init.write("init\n");
                fw_init.write("init\n");
                fw_init.flush();
                fw_init.close();

            }

        }

    }

    @FXML
    private void new_accpwd(ActionEvent event) throws IOException { //首頁切換到新增的頁面
        root = FXMLLoader.load(getClass().getResource("scene_newAccPwd.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    private void newAccPwd_new(ActionEvent event) throws IOException { //寫入要新增的帳號密碼

        FileWriter fw = new FileWriter(file, true);

        fw.append(new_acc.getText() + "\n");
        fw.append(new_pwd.getText() + "\n");
        fw.append(new_net.getText() + "\n");
        fw.flush();
        fw.close();
        newaccpwd_hint.setText("新增成功 ! 返回密碼庫");

        FileReader fr_login = new FileReader(file); //更新密碼管理的資料
        BufferedReader br_login = new BufferedReader(fr_login);
        while (br_login.ready()) {
            for (int x = 0; x < 20; x++) {
                for (int y = 0; y < 3; y++) {
                    if (x + 1 == 20) {
                        continue;
                    }
                    accpwd[x][y] = br_login.readLine();
                    if (accpwd[x][y] == null) {
                        accpwd[x][y] = "";
                    }
                    accpwd_listview.getItems().addAll("網站：" + accpwd[x + 1][2] + "\n" + "帳號：" + accpwd[x + 1][0] + "\n" + "密碼：" + accpwd[x + 1][0] + "\n");
                    accpwd_listview.refresh();
                }
            }
        }
        fr_login.close();

    }

    @FXML
    private void switchToScene_main(ActionEvent event) throws IOException {  //新增>>返回主頁 按鈕
        root = FXMLLoader.load(getClass().getResource("scene_login.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
